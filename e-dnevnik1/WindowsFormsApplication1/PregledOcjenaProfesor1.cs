﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class PregledOcjenaProfesor1 : Form
    {
        public PregledOcjenaProfesor1()
        {
            InitializeComponent();
        }

        String id_ucenika = PregledOcjenaProfesor.ID_ucenika;
        String konekcionistr = Login.konekcionistring;

        private void PregledOcjenaProfesor1_Load(object sender, EventArgs e)
        {
           PopuniPredmete();
           SveOcjene();
            comboBox1.SelectedItem = "Svi";


        }

        private void PopuniPredmete()
        {
            try
            {
                String query = "SELECT DISTINCT p.naziv_predmeta " +
                                " FROM ocjena o,  predmeti p " +
                                " WHERE p.id_predmeta=o.id_predmeta AND o.id_korisnika='" + id_ucenika + "' ";
                MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    comboBox1.Items.Add(reader["naziv_predmeta"].ToString());
                reader.Close();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        
        }
        private void SveOcjene()
        {
            try
            {
                String query = "SELECT p.naziv_predmeta AS 'Naziv predmeta', o.datum, o.ocjena " +
                                " FROM ocjena o, predmeti p " +
                                 " WHERE o.id_predmeta=p.id_predmeta AND o.id_korisnika='" + id_ucenika + "' " +
                                    " ORDER BY o.datum DESC ";

                MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                konekcija.Open();
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, konekcija);
                DataTable tabela = new DataTable();
                adapter.Fill(tabela);
                dataGridView1.DataSource = tabela;
                adapter.Dispose();
                konekcija.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.Text == "Svi")
                    SveOcjene();
                else Filter();
            }
                catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                }
           
            
        }


        private void Filter()
        {
            try
            {
                String query = "SELECT  p.naziv_predmeta AS 'Naziv predmeta', o.datum, o.ocjena " +
                                 " FROM ocjena o, predmeti p " +
                                  " WHERE o.id_predmeta=p.id_predmeta AND o.id_korisnika='" + id_ucenika + "' AND p.naziv_predmeta='"+comboBox1.Text+"' " +
                                     " ORDER BY o.datum DESC ";

                MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                konekcija.Open();
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, konekcija);
                DataTable tabela = new DataTable();
                adapter.Fill(tabela);
                dataGridView1.DataSource = tabela;
                adapter.Dispose();
                konekcija.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }


    }
}
