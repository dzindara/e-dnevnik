﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;


namespace WindowsFormsApplication1
{
    public partial class PrikazOcjena : Form
    {
        public PrikazOcjena()
        {
            InitializeComponent();
        }

       String konekionistr=Login.konekcionistring;

        private void unosPodatakaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            UnosPodataka UP = new UnosPodataka();
            this.Hide();
            UP.Show();
        }

      
        private void prikazSvihKorisnikaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            PrikazKorisnika PK = new PrikazKorisnika();
            this.Hide();
            PK.Show();
        }

        private void odjavaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Login Log = new Login();
            this.Hide();
            Log.PostaviLogoutStats();
            Log.Show();
        }

        private void izlazIzAplikacijeToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
            Login Log = new Login();
            Log.PostaviLogoutStats();
        }

        private void PrikazOcjena_Load(object sender, EventArgs e)
        {
            PrikazOcjena1();
            PopuniPredmete();
            comboBox1.SelectedItem = "Svi";
        }

        private void PrikazOcjena1()
        {
            try
            {
                String query = "SELECT o.ID_ocjene, k.ime, k.prezime, p.naziv_predmeta, o.datum, o.ocjena " +
                                "FROM ocjena o, korisnici k, predmeti p " +
                                " WHERE k.id_korisnika=o.id_korisnika AND p.id_predmeta=o.id_predmeta " ;
                MySqlConnection konekcija = new MySqlConnection(konekionistr);
               konekcija.Open();
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, konekcija);
                DataTable tabela = new DataTable();
                adapter.Fill(tabela);
                dataGridView1.DataSource = tabela;
                adapter.Dispose();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            Filter();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Filter();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
          
          Filter();
        }

        private void Filter()
        {

            try
            {
                String query = "SELECT ID_ocjene, ime, prezime , naziv_predmeta, datum, ocjena " +
                                " FROM ocjena o, korisnici k, predmeti p " +
                                " WHERE k.id_korisnika=o.id_korisnika AND p.id_predmeta=o.id_predmeta ";
                if (textBox1.Text != "")
                    query += " AND k.prezime LIKE '" + textBox1.Text + "%' ";               
                if (textBox3.Text != "")
                    query += " AND k.ime LIKE '" + textBox3.Text + "%' ";
                if (comboBox1.Text != "Svi")
                    query += " AND p.naziv_predmeta LIKE '" + comboBox1.Text + "' ";
              else   query += " AND p.naziv_predmeta LIKE '%' ";

                query += " ORDER BY k.id_korisnika ASC";
                MySqlConnection konekcija = new MySqlConnection(konekionistr);
                konekcija.Open();
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, konekcija);
                DataTable tabela = new DataTable();
                adapter.Fill(tabela);
                dataGridView1.DataSource = tabela;
                adapter.Dispose();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void PopuniPredmete()
        {
            try
            {
                String query = "SELECT naziv_predmeta FROM predmeti ";
                MySqlConnection konekcija = new MySqlConnection(konekionistr);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    comboBox1.Items.Add(reader["naziv_predmeta"].ToString());
                reader.Close();
                konekcija.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

        }

        private void meniToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void PrikazOcjena_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
            Login Log = new Login();
            Log.PostaviLogoutStats();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BrisanjeOcjene();
        }
        private void BrisanjeOcjene()
        {
            try
            {
                if (textBox2.Text == "")
                    errorProvider1.SetError(textBox2, "Niste unijeli ID ocjene!");
                else
                {
                    errorProvider1.Dispose();
                    String query = " DELETE FROM ocjena WHERE id_ocjene='" + textBox2.Text + "' ";
                    MySqlConnection konekcija = new MySqlConnection(konekionistr);
                    konekcija.Open();
                    MySqlCommand cmd = new MySqlCommand(query, konekcija);
                    cmd.ExecuteNonQuery();
                    konekcija.Close();
                    MessageBox.Show("Ocjena sa ID-om:" + textBox2.Text + " je obrisana!");
                    PrikazOcjena1();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
