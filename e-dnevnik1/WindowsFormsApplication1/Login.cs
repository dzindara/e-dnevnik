﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        public static String korisnikid;
        public static String konekcionistring= "Server=localhost; Port=3306; Database=e-dnevnik;" +
        "Uid=root; Pwd=root " ;


        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Visible = false;
            label2.Visible = false;
            textBoxuser.Visible = false;
            textBoxPw.Visible = false;
            prijava2.Visible = false;

            

        }

        private void prijava_Click(object sender, EventArgs e)
        {
            prijava.Visible = false;
            label1.Visible = true;
            label2.Visible = true;
            textBoxuser.Visible = true;
            textBoxPw.Visible = true;
            prijava2.Visible = true;
            label3.Visible = false;
        }

        private void prijava2_Click(object sender, EventArgs e)
        {
            Login1();
        }

        private void Login1()
        {
            errorProvider1.Clear();

            String usern = textBoxuser.Text;
            String passw = textBoxPw.Text;
            String query = "SELECT password, CONCAT(ime,' ',prezime),  id_korisnika ,  login_status , id_posla " +
                            "FROM korisnici WHERE username='" + usern + "' ";


            try
            {
                MySqlConnection konekcija = new MySqlConnection(konekcionistring);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                MySqlDataReader reader;
                reader = cmd.ExecuteReader();
                reader.Read();

                if (!reader.HasRows)
                {
                    errorProvider1.SetError(textBoxuser, "Username ne postoji!");
                }
                else
                {
                    String passwd = reader[0].ToString();
                    String imeiprez = reader[1].ToString();
                    korisnikid = reader[2].ToString();
                    String loginstats = reader[3].ToString();
                    String idposla = reader[4].ToString();

                    if (loginstats == "1")
                        errorProvider1.SetError(prijava2, "Korisnik je logovan!");
                    else if (passw == passwd && idposla == "3")
                    {
                        MessageBox.Show("Uspješno ste logovani! \nDobrodošli " + imeiprez);
                        PostaviLoginStats();
                        PregledOcjenaUcenik POU = new PregledOcjenaUcenik();
                        this.Hide();
                        POU.Show();
                    }
                    else if (passw == passwd && idposla == "2")
                    {
                        MessageBox.Show("Uspješno ste logovani! \nDobrodošli " + imeiprez);
                        PostaviLoginStats();
                        UnosOcjeneProfesor UOP = new UnosOcjeneProfesor();
                        this.Hide();
                        UOP.Show();
                    }
                    else if (passw == passwd && idposla == "1")
                    {
                        MessageBox.Show("Uspješno ste logovani! \nDobrodošli " + imeiprez);
                        PostaviLoginStats();
                        UnosPodataka UP = new UnosPodataka();
                        this.Hide();
                        UP.Show();
                    }
                    else errorProvider1.SetError(textBoxPw, "Pogrešan password!");
                }
                reader.Close();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

        }

        private void PostaviLoginStats()
        {
            String query = "UPDATE korisnici SET login_status=1 " +
                            "WHERE id_korisnika='" + korisnikid + "' ";

            try
            {
                MySqlConnection konekcija = new MySqlConnection(konekcionistring);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                cmd.ExecuteNonQuery();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void PostaviLogoutStats()
        { 
            String query ="UPDATE korisnici SET login_status=0 " +
                            "WHERE id_korisnika='" + korisnikid + "' ";

            try
            {
                MySqlConnection konekcija = new MySqlConnection(konekcionistring);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                cmd.ExecuteNonQuery();
                konekcija.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
