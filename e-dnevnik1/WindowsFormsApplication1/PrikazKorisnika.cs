﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;


namespace WindowsFormsApplication1
{
    public partial class PrikazKorisnika : Form
    {
        public PrikazKorisnika()
        {
            InitializeComponent();

      
        }

        String konekcionstr = Login.konekcionistring;
       
        private void unosPodatakaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnosPodataka UP = new UnosPodataka();
                this.Hide();
            UP.Show();

        }

      

        private void prikazOcjenaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrikazOcjena PO = new PrikazOcjena();
            this.Hide();
            PO.Show();
        }

        private void odjavaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login Log = new Login();
            this.Hide();
            Log.PostaviLogoutStats();
            Log.Show();
        }

        private void izlazIzAplikacijeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
            Login Log = new Login();
            
            Log.PostaviLogoutStats();
        }

        private void PrikazKorisnika_Load(object sender, EventArgs e)
        {
            PopuniProfesije();
            PrikazKorisnika1();
            comboBox1.SelectedItem = "Svi";
           
            
        }

        private void PrikazKorisnika1()
        {
            try
            {

                String query = "SELECT k.id_korisnika, p.naziv_posla AS 'Tip korisnika' , k.ime, k.prezime, k.username, k.password " +
                                " FROM korisnici k, poslovi p WHERE k.id_posla=p.id_posla ";
                MySqlConnection konekcija = new MySqlConnection(konekcionstr);
                konekcija.Open();
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, konekcija);
                DataTable tabela = new DataTable();
                adapter.Fill(tabela);
                dataGridView1.DataSource = tabela;
                adapter.Dispose();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       

        

        private void PopuniProfesije()
        {
            try
            {
                String query = "SELECT naziv_posla FROM poslovi ";
                MySqlConnection konekcija = new MySqlConnection(konekcionstr);
                konekcija.Open();
                MySqlCommand cmd= new MySqlCommand(query,konekcija);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    comboBox1.Items.Add(reader["naziv_posla"].ToString());
                reader.Close();
                konekcija.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        
        }

      

        private void Filter()
        {

            try
            {
                String query = "SELECT k.id_korisnika, p.naziv_posla AS 'Tip korisnika' , k.ime, k.prezime, k.username, k.password " +
                                 " FROM korisnici k, poslovi p WHERE k.id_posla=p.id_posla ";
                if (textBox1.Text != "")
                    query += " AND k.id_korisnika LIKE '" + textBox1.Text + "%' ";
                if (textBox2.Text != "")
                    query += " AND k.ime LIKE '" + textBox2.Text + "%' ";
                if (textBox3.Text != "")
                    query += " AND k.prezime LIKE '" + textBox3.Text + "%' ";
                if (comboBox1.Text != "Svi")
                    query += " AND p.naziv_posla LIKE '" + comboBox1.Text + "' ";
                else query += " AND p.naziv_posla LIKE '%' ";




                query += " ORDER BY k.id_korisnika ASC";
                MySqlConnection konekcija = new MySqlConnection(konekcionstr);
                konekcija.Open();
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, konekcija);
                DataTable tabela = new DataTable();
                adapter.Fill(tabela);
                dataGridView1.DataSource = tabela;
                adapter.Dispose();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PrikazKorisnika_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
            Login Log = new Login();
            Log.PostaviLogoutStats();
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            Filter();
        }

        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {
            Filter();
        }

        private void textBox3_TextChanged_1(object sender, EventArgs e)
        {
            Filter();
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        
             Filter();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox4.Text == "")
                    errorProvider1.SetError(textBox4, "Niste unijeli ID korisnika!");
                else
                {
                    errorProvider1.Dispose();
                    String query = "DELETE FROM  korisnici WHERE id_korisnika='" + textBox4.Text + "' ";
                    MySqlConnection konekcija = new MySqlConnection(konekcionstr);
                    konekcija.Open();
                    MySqlCommand cmd = new MySqlCommand(query, konekcija);
                    cmd.ExecuteNonQuery();
                    konekcija.Close();
                    MessageBox.Show("Korisnik sa ID-om:" + textBox4.Text + " je obrisan!");
                    textBox1.Clear();
                    textBox2.Clear();
                    textBox3.Clear();
                    textBox4.Clear();
                    PrikazKorisnika1();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
        }
}
