﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class UnosOcjeneProfesor : Form
    {
        public UnosOcjeneProfesor()
        {
            InitializeComponent();
        }

        String konekcionistring1 = Login.konekcionistring;
        String idProf = Login.korisnikid;

        private void odjavaIzAplikacijeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnosOcjeneProfesor UOP = new UnosOcjeneProfesor();
            UOP.Hide();
            Login Log = new Login();
            Log.PostaviLogoutStats();
            Log.Show();

        }

        private void izlazIzAplikacijeToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Login Log = new Login();
            Log.PostaviLogoutStats();
            Application.Exit();
        }

        

        private void pregledaOcjenaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PregledOcjenaProfesor POP = new PregledOcjenaProfesor();
            this.Hide();
            POP.Show();
        }

        private void UnosOcjeneProfesor_FormClosed(object sender, FormClosedEventArgs e)
        {

            Login Log = new Login();
            Log.PostaviLogoutStats();
            Application.Exit();
        }

        private void UnosOcjeneProfesor_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedItem = "I-1";
            PredavanjaProf();
            
            comboBox4.SelectedItem = "1";
        }



        private void ListaUcenika()
        {
            try
            {
                String query = "SELECT CONCAT(prezime,' ',ime) " +
                            "FROM korisnici WHERE razred='" + comboBox1.Text + "' ORDER BY CONCAT(prezime,' ',ime) ASC ";

                MySqlConnection konekcija = new MySqlConnection(konekcionistring1);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    comboBox2.Items.Add(reader["CONCAT(prezime,' ',ime)"].ToString());
                
                reader.Close();
                konekcija.Close();
             
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            IDUcenika();
        }

        private void IDUcenika()
        {
            try
            {
                String query = "SELECT id_korisnika FROM korisnici "+
                            " WHERE CONCAT(prezime,' ',ime)='"+comboBox2.Text+ "'";
                MySqlConnection konekcija = new MySqlConnection(konekcionistring1);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                textBox1.Text = reader["id_korisnika"].ToString();

                reader.Close();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            ListaUcenika();
        }

        private void PredavanjaProf()
        {
            try
            {
                String query = "SELECT p.naziv_predmeta FROM predmeti p, predavanja pr " +
                    "WHERE p.id_predmeta=pr.id_predmeta AND pr.id_korisnika='" + idProf + "'";
                MySqlConnection konekcija = new MySqlConnection(konekcionistring1);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    comboBox3.Items.Add(reader["naziv_predmeta"].ToString());
                
                reader.Close();
                
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        
        }

       String IDOPred;

       private void IDOPredmeta()
       {
           try
           {
               String query= "SELECT id_predmeta FROM predmeti WHERE naziv_predmeta='"+comboBox3.Text+"'";
               MySqlConnection konekcija = new MySqlConnection(konekcionistring1);
               konekcija.Open();
               MySqlCommand cmd = new MySqlCommand(query, konekcija);
               MySqlDataReader reader = cmd.ExecuteReader();
               reader.Read();
               IDOPred = reader[0].ToString();
               reader.Close();
               konekcija.Close();
             
           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
       }

       private void button1_Click(object sender, EventArgs e)
       {
           errorProvider1.Clear();
           if (comboBox2.Text == "")
               errorProvider1.SetError(comboBox2, "Niste odabrali učenika!");
           else if (comboBox3.Text == "")
               errorProvider1.SetError(comboBox3, "Niste odabrali predmet!");
           else
           UnosOcjene();
       }


       private void UnosOcjene()
       {
           try
           {
               String query = "INSERT INTO ocjena(id_korisnika, datum, ocjena, id_predmeta) " +
                                "VALUES ('" + textBox1.Text + "','" + dateTimePicker1.Value.ToString("yyyy-MM-dd") + "','" + comboBox4.Text + "','" + IDOPred + "') ";
               MySqlConnection konekcija = new MySqlConnection(konekcionistring1);
               konekcija.Open();
               MySqlCommand cmd = new MySqlCommand(query, konekcija);
               cmd.ExecuteNonQuery();
               konekcija.Close();
               MessageBox.Show("Unijeli ste ocjenu "+comboBox4.Text+" učeniku "+comboBox2.Text+" na datum "+dateTimePicker1.Value.ToString("yyyy-MM-dd")+" iz predmeta "+comboBox3.Text+".");
           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
       }

       private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
       {
           IDOPredmeta();
       }

       private void groupBox2_Enter(object sender, EventArgs e)
       {

       }



    }
}
