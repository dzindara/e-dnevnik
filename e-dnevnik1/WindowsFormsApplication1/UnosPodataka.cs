﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class UnosPodataka : Form
    {
        public UnosPodataka()
        {
            InitializeComponent();
        }

        string konekcionistr = Login.konekcionistring;


      

      

        private void prikazSvihKorisnikaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrikazKorisnika PK = new PrikazKorisnika();
            this.Hide();
            PK.Show();
        }

        private void prikazOcjenaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrikazOcjena PO = new PrikazOcjena();
            this.Hide();
            PO.Show();
        }

        private void odjavaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login Log = new Login();
            this.Hide();
            Log.PostaviLogoutStats();
            Log.Show();
        }

        private void izlazIzAplikacijeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
            Login Log = new Login();
            Log.PostaviLogoutStats();
        }

        private void UnosPodataka_Load(object sender, EventArgs e)
        {
            ispuniCheckedListbox();
            ListaPredmeta();
        }

      

        private void ispuniCheckedListbox()
        {
            try
            {
            String query = "SELECT id_predmeta, naziv_predmeta FROM predmeti";

            MySqlConnection konekcija = new MySqlConnection(konekcionistr);
            konekcija.Open();
            MySqlCommand cmd = new MySqlCommand(query,konekcija);
            MySqlDataReader reader;
            reader=cmd.ExecuteReader();
            while (reader.Read())
            {
                checkedListBox1.Items.Add(reader["naziv_predmeta"].ToString());
            }
            reader.Close();
            konekcija.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            UnesiProf();
        }

        private void UnesiProf()
        {
            errorProvider1.Clear();
            try
            {
                if (textBox1.Text == "")
                   errorProvider1.SetError(textBox1, "Niste unijeli ime!");
                else if (textBox2.Text == "")
                   errorProvider1.SetError(textBox2, "Niste unijeli prezime!");
                else if (textBox3.Text == "")
                   errorProvider1.SetError(textBox3, "Niste unijeli username!");
                else if (textBox4.Text == "")
                    errorProvider1.SetError(textBox4, "Niste unijeli password!");
                else if (checkedListBox1.Text== "")
                    errorProvider1.SetError(checkedListBox1, "Niste odabrali nijedan predmet!");
                else
                {
                   
                        String query = "INSERT INTO korisnici(id_posla, ime, prezime, username, password, login_status) " +
                                        " VALUES (2,'" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "',0)";
                        MySqlConnection konekc = new MySqlConnection(konekcionistr);
                        konekc.Open();
                        MySqlCommand cmd = new MySqlCommand(query, konekc);
                        cmd.ExecuteNonQuery();
                        konekc.Close();
                        PredavanjaProfesora();
                        MessageBox.Show("Profesor/ica " + textBox1.Text + " " + textBox2.Text + " je dodan/a!");
                        textBox1.Clear();
                        textBox2.Clear();                        
                        textBox3.Clear();
                        textBox4.Clear();
                        for (int i = 0; i < checkedListBox1.Items.Count; i++)
                        {
                            checkedListBox1.SetItemCheckState(i, (CheckState.Unchecked));
                        }              
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        String predmet;
        private void PredavanjaProfesora()
        {
            try
            {
                 foreach (object predavanje in checkedListBox1.CheckedItems)
            {
                predmet = predavanje.ToString();
                IDpredmeta();
                IDprofesora();

                String query = "INSERT INTO predavanja (id_korisnika, id_predmeta) " +
                                " VALUES ('"+ID_profesora+"', '"+ID_predmeta+"') " ;
                MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                cmd.ExecuteNonQuery();
                konekcija.Close();               
            }
            }
           catch(Exception ex)
            {
            MessageBox.Show(ex.Message);
            }            
        }

        String ID_profesora;
        private void IDprofesora()
        {
            try
            {
                String query = "SELECT id_korisnika " +
                               " FROM korisnici WHERE username='" + textBox3.Text + "'";

                MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                ID_profesora = reader[0].ToString();
                reader.Close();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        String ID_predmeta;
        private void IDpredmeta()
        {
            try
            {
                String query = "SELECT id_predmeta FROM predmeti " +
                               " WHERE naziv_predmeta='" + predmet + "' ";

                MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                ID_predmeta = reader[0].ToString();
                reader.Close();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            UnesiUcenika();
        }

        private void UnesiUcenika()
        {
            errorProvider1.Clear();
            try
            {

                if (textBox5.Text == "")
                   errorProvider1.SetError(textBox5, "Niste unijeli ime!");
                else if (textBox6.Text == "")
                   errorProvider1.SetError(textBox6, "Niste unijeli prezime!");
                else if (comboBox1.Text == "")
                    errorProvider1.SetError(comboBox1, "Niste odabrali razred!");
                else if (textBox7.Text == "")
                   errorProvider1.SetError(textBox7, "Niste unijeli username!");
                else if (textBox8.Text == "")
                    errorProvider1.SetError(textBox8, "Niste unijeli password!");
                else
                {
                    String query = "INSERT INTO korisnici (id_posla, ime, prezime, razred, username, password, login_status) " +
                                " VALUES(3,'" + textBox5.Text + "','" + textBox6.Text + "','" + comboBox1.Text + "','" + textBox7.Text + "','" + textBox8.Text + "',0)";
                    MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                    konekcija.Open();
                    MySqlCommand cmd = new MySqlCommand(query, konekcija);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Učenik/ica " + textBox5.Text + " " + textBox6.Text + " je dodan/a!");
                    textBox5.Clear();
                    textBox6.Clear();
                    comboBox1.Text = "";
                    textBox7.Clear();
                    textBox8.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            UnosPredmeta();
            checkedListBox1.Items.Clear();
            ispuniCheckedListbox();
            comboBox2.Items.Clear();
            ListaPredmeta();
        }


        private void UnosPredmeta()
        {
            errorProvider1.Clear();
            try
            {
                if (textBox9.Text == "" || textBox9.Text == " ")
                    errorProvider1.SetError(textBox9, "Niste unijeli ime predmeta!");
                else
                {
                    String query = "INSERT INTO predmeti (naziv_predmeta) " +
                                   "VALUE ('" + textBox9.Text + "') ";

                    MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                    konekcija.Open();
                    MySqlCommand cmd = new MySqlCommand(query, konekcija);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Predmet " + textBox9.Text + " je dodan!");
                    textBox9.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UnosPodataka_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
              Login Log = new Login();
              Log.PostaviLogoutStats();
            
        }

        private void ListaPredmeta()
        {
            try
            {
                String query = "SELECT naziv_predmeta FROM predmeti ";
                MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    comboBox2.Items.Add(reader["naziv_predmeta"].ToString());
                }
                reader.Close();
                konekcija.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BrisanjePredmeta();
            checkedListBox1.Items.Clear();
            ispuniCheckedListbox();
        }
        private void BrisanjePredmeta()
        {
            try
            {
                if (comboBox2.Text == "")
                    errorProvider1.SetError(comboBox2, "Niste odabrali predmet!");
                else
                {
                    errorProvider1.Dispose();
                    String query = " DELETE FROM predmeti WHERE naziv_predmeta='" + comboBox2.SelectedItem + "' ";

                    MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                    konekcija.Open();
                    MySqlCommand cmd = new MySqlCommand(query, konekcija);
                    cmd.ExecuteNonQuery();
                    konekcija.Close();
                    MessageBox.Show("Predmet " + comboBox2.Text + " je obrisan!");
                    comboBox2.Text = "";
                    comboBox2.Items.Clear();
                    ListaPredmeta();
                    
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
