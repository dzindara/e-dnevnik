﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class PregledOcjenaProfesor : Form
    {
        public PregledOcjenaProfesor()
        {
            InitializeComponent();
        }

        String konekcionistr = Login.konekcionistring;

        private void UnosOcjenaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnosOcjeneProfesor UOP = new UnosOcjeneProfesor();
            this.Hide();
            UOP.Show();
        }

        private void odjavaIzAplikacijeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Login Log = new Login();
            this.Hide();
            Log.PostaviLogoutStats();
            Log.Show();
          
        }

        private void izlazIzAplikacijeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login Log = new Login();
            Log.PostaviLogoutStats();
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (comboBox2.Text == "")
                errorProvider1.SetError(comboBox2, "Niste odabrali učenika!");
            else
            {
                PregledOcjenaProfesor1 POP1 = new PregledOcjenaProfesor1();
                POP1.Show();
            }
            
        }

        private void PregledOcjenaProfesor_FormClosed(object sender, FormClosedEventArgs e)
        {

            Login Log = new Login();
            Log.PostaviLogoutStats();
            Application.Exit();
        }

        private void PregledOcjenaProfesor_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedItem = "I-1";
        }

        private void IiPU()
        {
                try
                {
                    String query = "SELECT CONCAT(prezime,' ',ime) FROM korisnici " +
                                "WHERE razred='" + comboBox1.Text + "' ORDER BY CONCAT(prezime,' ',ime) ASC ";

                    MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                    konekcija.Open();
                    MySqlCommand cmd = new MySqlCommand(query, konekcija);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                        comboBox2.Items.Add(reader["CONCAT(prezime,' ',ime)"].ToString());


                    reader.Close();
                    konekcija.Close();

                }
            catch(Exception ex)
                {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            IiPU();
           
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            IDUcenika();
           
        }

        public static String ID_ucenika;

        private void IDUcenika()
        {
            try
            {
                String query = "SELECT id_korisnika FROM korisnici " +
                                "WHERE CONCAT(prezime,' ',ime)='" + comboBox2.Text + "'";

                MySqlConnection konekcija = new MySqlConnection(konekcionistr);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                ID_ucenika = reader[0].ToString();
                reader.Close();
                konekcija.Close();
            }
               
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
