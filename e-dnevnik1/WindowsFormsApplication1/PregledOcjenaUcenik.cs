﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class PregledOcjenaUcenik : Form
    {
        public PregledOcjenaUcenik()
        {
            InitializeComponent();
        }

        String konekcioni = Login.konekcionistring;
        String UcenikID = Login.korisnikid;

        private void odjavaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login Log = new Login();
            this.Hide();
            Log.PostaviLogoutStats();
            Log.Show();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login Log = new Login();
            Log.PostaviLogoutStats();
            Application.Exit();
      
            
        }

        private void PregledOcjenaUcenik_FormClosed(object sender, FormClosedEventArgs e)
        {
            Login Log = new Login();
            Log.PostaviLogoutStats();
            Application.Exit();
        }

        private void PregledOcjenaUcenik_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedItem = "Svi";
            IspisSvihOcjena();
            popuniComboBox();
        }

        private void IspisSvihOcjena()
        {
            try
            {
                String query = "SELECT p.naziv_predmeta AS 'Naziv predmeta', o.datum, o.ocjena " +
                                " FROM ocjena o, predmeti p " +
                                 " WHERE o.id_predmeta=p.id_predmeta AND o.id_korisnika='" + UcenikID + "' " +
                                    " ORDER BY o.datum DESC ";

                MySqlConnection konekcija = new MySqlConnection(konekcioni);
                konekcija.Open();
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, konekcija);
                DataTable tabela = new DataTable();
                adapter.Fill(tabela);
                dataGridView1.DataSource = tabela;
                adapter.Dispose();
                konekcija.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void popuniComboBox()
        {
            try
            {
                String query= "SELECT DISTINCT p.naziv_predmeta "+
                                " FROM ocjena o,  predmeti p "+
                                " WHERE p.id_predmeta=o.id_predmeta AND o.id_korisnika='"+UcenikID+"' ";
                MySqlConnection konekcija = new MySqlConnection(konekcioni);
                konekcija.Open();
                MySqlCommand cmd = new MySqlCommand(query, konekcija);
                MySqlDataReader reader = cmd.ExecuteReader();
               
                while (reader.Read())
                    comboBox1.Items.Add(reader["naziv_predmeta"].ToString());
                reader.Close();
                konekcija.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filter();
        }

        private void Filter()
        {
            try
            {
                String query = "SELECT p.naziv_predmeta AS 'Naziv predmeta', o.datum, o.ocjena " +
                               " FROM ocjena o, predmeti p " +
                               " WHERE o.id_predmeta=p.id_predmeta AND o.id_korisnika='" + UcenikID + "'  ";

                if (comboBox1.Text == "Svi") 
                query +=" AND p.naziv_predmeta LIKE '%' ";
                else query += " AND p.naziv_predmeta LIKE '"+comboBox1.Text+ "' ";

                      query+= " ORDER BY o.datum DESC ";

                MySqlConnection konekcija = new MySqlConnection(konekcioni);
                konekcija.Open();
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, konekcija);
                DataTable tabela = new DataTable();
                adapter.Fill(tabela);
                dataGridView1.DataSource = tabela;
                adapter.Dispose();
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


    }
}
