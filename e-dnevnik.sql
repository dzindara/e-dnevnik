-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2016 at 01:52 AM
-- Server version: 5.05.01
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `e-dnevnik`
--

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

DROP TABLE IF EXISTS `korisnici`;
CREATE TABLE IF NOT EXISTS `korisnici` (
  `id_korisnika` int(11) NOT NULL auto_increment,
  `id_posla` int(2) NOT NULL,
  `ime` text NOT NULL,
  `prezime` text CHARACTER SET utf8 NOT NULL,
  `razred` text,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `login_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_korisnika`),
  KEY `korisnici_ibfk_1` (`id_posla`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`id_korisnika`, `id_posla`, `ime`, `prezime`, `razred`, `username`, `password`, `login_status`) VALUES
(1, 1, 'Nedžad', 'Džindo', NULL, 'admin', 'admin', 0),
(2, 1, 'Adnan', 'Delić', NULL, 'admin1', 'admin1', 0),
(3, 2, 'Marina', 'Cvitanović', NULL, '1marina', '1marina.c', 0),
(4, 2, 'Nejra', 'Mulaosmanović', NULL, '2nejra', '2nejra.m', 0),
(5, 2, 'Fikret', 'Radoncić', NULL, '3fikret', '3fikret.r', 0),
(6, 2, 'Melina', 'Fazlić', NULL, '4melina', '4melina.f', 0),
(7, 2, 'Maida', 'Kunduklija', NULL, '5maida', '5maida.k', 0),
(8, 2, 'Rifat', 'Fakić', NULL, '6rifat', '6rifat.f', 0),
(9, 2, 'Aida', 'Halilović', NULL, '7aida', '7aida.h', 0),
(10, 2, 'Adnan', 'Delić', NULL, '8adnan', '8adnan.d', 0),
(11, 2, 'Amir', 'Karačić', NULL, '9amir', '9amir.k', 0),
(12, 2, 'Faruk', 'Kurtović', NULL, '10faruk', '10faruk.k', 0),
(13, 2, 'Mustafa', 'Imširević', NULL, '11mustafa', '11mustafa.i', 0),
(14, 2, 'Admir', 'Demir', NULL, '12admir', '12admir.d', 0),
(15, 2, 'Sanela', 'Hrnjić', NULL, '13sanela', '13sanela.h', 0),
(16, 3, 'Stefan', 'Balaban', 'IV-2', '1stefan', '1stefan.b', 0),
(17, 3, 'Robert', 'Buzuk', 'IV-2', '2robert', '2robert.b', 0),
(18, 3, 'Azur', 'Čaušević', 'IV-2', '3azur', '3azur.c', 0),
(19, 3, 'Tarik', 'Duraković', 'IV-2', '4tarik', '4tarik.d', 0),
(20, 3, 'Nedžad', 'Džindo', 'IV-2', '5nedzad', '5nedzad.d', 0),
(21, 3, 'Ibrahim', 'Husović', 'IV-1', '6ibrahim', '6ibrahim.h', 0),
(22, 3, 'Tarik', 'Bučan', 'IV-1', '7tarik', '7tarik.b', 0),
(23, 3, 'Namik', 'Karić', 'IV-1', '8namik', '8namik.k', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ocjena`
--

DROP TABLE IF EXISTS `ocjena`;
CREATE TABLE IF NOT EXISTS `ocjena` (
  `id_ocjene` int(10) NOT NULL auto_increment,
  `id_korisnika` int(10) NOT NULL,
  `datum` date NOT NULL,
  `ocjena` int(1) NOT NULL,
  `id_predmeta` int(244) NOT NULL,
  PRIMARY KEY (`id_ocjene`),
  KEY `id_predmeta` (`id_predmeta`),
  KEY `ocjena_ibfk_1` (`id_korisnika`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ocjena`
--

INSERT INTO `ocjena` (`id_ocjene`, `id_korisnika`, `datum`, `ocjena`, `id_predmeta`) VALUES
(1, 22, '2016-04-21', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `poslovi`
--

DROP TABLE IF EXISTS `poslovi`;
CREATE TABLE IF NOT EXISTS `poslovi` (
  `id_posla` int(2) NOT NULL auto_increment,
  `naziv_posla` varchar(35) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_posla`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poslovi`
--

INSERT INTO `poslovi` (`id_posla`, `naziv_posla`) VALUES
(1, 'Administator'),
(2, 'Profesor'),
(3, 'Učenik');

-- --------------------------------------------------------

--
-- Table structure for table `predavanja`
--

DROP TABLE IF EXISTS `predavanja`;
CREATE TABLE IF NOT EXISTS `predavanja` (
  `id_predavanja` int(3) NOT NULL auto_increment,
  `id_predmeta` int(2) NOT NULL,
  `id_korisnika` int(11) NOT NULL,
  PRIMARY KEY (`id_predavanja`),
  KEY `id_predmeta` (`id_predmeta`),
  KEY `predavanja_ibfk_2` (`id_korisnika`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `predavanja`
--

INSERT INTO `predavanja` (`id_predavanja`, `id_predmeta`, `id_korisnika`) VALUES
(1, 1, 3),
(2, 2, 4),
(3, 3, 5),
(4, 4, 6),
(5, 5, 7),
(6, 6, 8),
(7, 7, 9),
(8, 8, 10),
(9, 9, 11),
(10, 12, 11),
(11, 10, 12),
(12, 11, 13),
(13, 12, 1),
(14, 14, 15);

-- --------------------------------------------------------

--
-- Table structure for table `predmeti`
--

DROP TABLE IF EXISTS `predmeti`;
CREATE TABLE IF NOT EXISTS `predmeti` (
  `id_predmeta` int(2) NOT NULL auto_increment,
  `naziv_predmeta` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_predmeta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `predmeti`
--

INSERT INTO `predmeti` (`id_predmeta`, `naziv_predmeta`) VALUES
(1, 'Bosanski jezik'),
(2, 'Engleski jezik'),
(3, 'Tjelesni i zdravstveni odgoj'),
(4, 'Sociologija'),
(5, 'Matematika'),
(6, 'Elektronska mjerenja'),
(7, 'Električna kola'),
(8, 'Programiranje i programski jezici'),
(9, 'Relacione baze podataka'),
(10, 'Računarske mreže'),
(11, 'Digitalne računske mašine'),
(12, 'Labaratorijski rad'),
(14, 'Vjeronauka');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD CONSTRAINT `korisnici_ibfk_1` FOREIGN KEY (`id_posla`) REFERENCES `poslovi` (`id_posla`);

--
-- Constraints for table `ocjena`
--
ALTER TABLE `ocjena`
  ADD CONSTRAINT `ocjena_ibfk_1` FOREIGN KEY (`id_korisnika`) REFERENCES `korisnici` (`id_korisnika`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ocjena_ibfk_2` FOREIGN KEY (`id_predmeta`) REFERENCES `predmeti` (`id_predmeta`);

--
-- Constraints for table `predavanja`
--
ALTER TABLE `predavanja`
  ADD CONSTRAINT `predavanja_ibfk_1` FOREIGN KEY (`id_predmeta`) REFERENCES `predmeti` (`id_predmeta`),
  ADD CONSTRAINT `predavanja_ibfk_2` FOREIGN KEY (`id_korisnika`) REFERENCES `korisnici` (`id_korisnika`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
